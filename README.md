# PreloadLib

#### 介绍
[性能优化之]Android-布局离屏预加载&&Android-WebView预渲染：
- 1，复杂页面[如首页Activity]布局，在欢迎页执行离屏预加载。之后在跳转到页面时，直接使用已加载、请求和处理的View和数据，从而提高页面显示速度避免白屏。
- 2，WebView首次使用显示时白屏，通过WebView预渲染、缓存，并使用解决。

#### 软件架构
软件架构说明


#### 安装教程

1.   **settings.gradle**  配置添加：`include ':preloadlib'`
2.  app-module下 **build.gradle**  配置添加：`implementation project(':preloadlib')`

#### 使用说明

需求由ActivityA 跳转到 ActivityB，对ActivityB进行离屏预加载。
- 在ActivityA中使用示例如下，编辑如下代码执行预加载、跳转逻辑。

```
// kotlin文件中
val intent: Intent = Intent(this.activity, MainBasePreloadActivity::class.java)
ActivityPreloadManager.preload(intent, this.requireContext(), R.layout.activity_main_preload) // 预加载View
btnPreload.setOnClickListener {
    startActivity(intent)
}
```

- 在ActivityB中使用示例如下，继承预加载基类。

```
// kotlin文件中 - ActivityB(MainBasePreloadActivity)
class MainBasePreloadActivity : BasePreloadActivity() {

    override fun getResId(): Int {
        return R.layout.activity_main_preload
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) // 不能完全重写父类onCreate方法

        // 点击布局 R.layout.activity_main_preload 中 按钮
        attempt_btn.setOnClickListener {
            Toast.makeText(this, "立即体验", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onStart() {
        super.onStart()
    }
}
```

