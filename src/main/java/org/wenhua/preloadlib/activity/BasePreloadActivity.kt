package org.wenhua.preloadlib.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlin.reflect.KClass

/** Activity基类AppCompatActivity可换。进入Activity之前布局未预加载时，在onCreate执行重新加载。 */
abstract class BasePreloadActivity: AppCompatActivity() {

    private lateinit var preloadBaseView: PreloadBundleView

    // KClass = Context::class
    // Class = Context::class.java
    private fun preloadViewClassReflect(): KClass<PreloadBundleView> {
        return PreloadBundleView::class
    }
    abstract fun getResId(): Int
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 该父类onCreate方法，不能完全被重写
        preloadBaseView = preloadViewClassReflect().createView(intent, this, getResId())
        setContentView(preloadBaseView) // 添加自定义ViewGroup到Activity
    }


}

