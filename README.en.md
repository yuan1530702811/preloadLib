# PreloadLib

#### Description
[性能优化之]Android-布局离屏预加载&&Android-WebView预渲染：1，复杂页面[如首页Activity]布局，在欢迎页执行离屏预加载。之后在跳转到页面时，直接使用已加载、请求和处理的View和数据，从而提高页面显示速度避免白屏。2，WebView首次使用显示时白屏，通过WebView预渲染、缓存，并使用解决。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
